
# Formation École d'été Text Mining

## Cas d'étude

Nous proposons dans la cadre de l'atelier d'explorer l'utilisation du pipeline PyMotifs à travers deux cas d'étude, l'un sur un corpus de très petite taille et un autre sur un corpus plus volumineux afin d'explorer différents usages de la librairie.

- Une étude du style d'Alain Damasio dans la *Horde du Contrevent*.
- Une étude de plusieurs romans de trois auteurs.

Commençons par celui sur la *Horde du Contrevent*.

## Damasio

# Présentation du corpus et constitution

## Résumé général : 

_La Horde du Contrevent_ est un roman de fantasy d'Alain Damasio publié en 2004. Il raconte l'histoire d'une équipe de 22 personnes, chacune dotée d'une compétence unique, qui voyage à travers un monde inhospitalier et venteux pour atteindre l'Extrême-Amont, d'où souffle le vent qui façonne leur monde. Leur mission est de découvrir l'origine de ce vent et de trouver un moyen de l'arrêter, afin de mettre fin aux terribles tempêtes qui ravagent leur monde depuis des siècles. 

Le roman est narré à travers les perspectives de différents membres de l'équipe, chacun avec leur propre voix et leur propre histoire.

Au fil de leur voyage, ils doivent faire face à de nombreux dangers, tant physiques que psychologiques, et apprendre à travailler ensemble malgré leurs différences et leurs conflits internes. La Horde doit également affronter les obstacles posés par les différents étages de la société qui contrôle le monde dans lequel ils vivent.

Le roman aborde des thèmes tels que la quête de sens, la lutte contre l'oppression, le pouvoir de l'amitié et de la solidarité.

## Explication sur le procédé narratif de la _Horde du Contrevent_

_La Horde du Contrevent_ fonctionne selon un procédé de narration polyphonique où chaque personnage développe sa propre voix narrative, identifiée au sein du roman par un système de glyphe en début de paragraphe permettant au lecteur de savoir quel personnage est en train de prendre en charge le récit. Chaque personnage a donc un glyphe particulier, ainsi qu'un rôle déterminé dans la Horde : 

- Ω pour Golgoth, Traceur.
- π pour Pietro Della Rocca, Prince.
- ) pour Sov Sevcenko Strochnis, Scribe.
- Etc.

![marque-page](media/perso.png)

![exemple-page](media/page-livre.png)

## Constitution du corpus : 

Nous avons pratiqué un découpage de l'ensemble du livre _La Horde du Contrevent_ en isolant chaque voix narrative correspondant aux 22 personnages du livre. 

Il est intéressant de noter d'entrée que sur les 22 personnages présentant des glyphes au début du roman, seuls 20 ont en charge une voix narrative propre dans le récit. Nous disposons donc de 20 fichiers .txt comprenant, dans l'ordre de déroulement du récit, chacune des voix des personnages. 

En plus de ce corpus correspondant aux récits des différents personnages, nous avons également isolé l'ensemble des dialogues pour constituer un deuxième corpus. Dans celui-ci, nous avons retiré les quelques incises dialogiques que Damasio a laissé ("dit-elle", "beugla x", etc.). Soulignons au passage que souvent (mais pas systématiquement), ces incises sont entre parenthèses, semblables à des didascalies théâtrales.

__N.B.__ : Dans la mesure où chaque voix narrative est, comme nous l'avons vu, identifiée par un glyphe permettant de comprendre à quelle focalisation nous passons, nous avons retiré les glyphes des différents corpus pour éviter que les motifs de chaque personnage soient influencé par ce glyphe qui leur est propre.

## Pistes d'analyse : 

- À quel point les motifs stylistiques sont distinctifs pour chaque locuteur ? Comment le rôle spécifique de chaque personnage dans le roman peut être également approché à partir du style de sa narration ?
- À quel point le projet d'Alain Damasio de créer un roman polyphonique à 22 voix parvient-il à remplir son objectif ? Est-ce que les personnages présentent, au niveau textométrique, une réelle distinction ?
- Quels sont les personnages les plus proches les uns des autres au niveau narratif ? Quels sont les personnages présentant le style le plus distinctif ?
- Le roman ne présentant quasiment pas de signes dialogiques permettant d'identifier les locuteurs au sein des dialogues - cette identification reposant en partie sur le style propre du dialogue et l'aptitude du lecteur à faire le lien entre la voix narrative et le style orale du dialogue -, est-il possible d'entraîner, à partir des arcs narratifs, un modèle de prédiction identifiant les locuteurs ?
  - À partir de ce modèle prédictif, quelles sont les motifs les plus distinctifs de chaque personnage ?

